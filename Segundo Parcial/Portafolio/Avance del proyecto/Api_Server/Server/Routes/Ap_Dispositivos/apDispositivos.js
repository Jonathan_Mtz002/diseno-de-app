const express = require('express');
const apDispositivos = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

apDispositivos.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM ap_dispositivos', (err, rows) =>{
            if (err) return res.send(err)

            res.json(rows)
        })
    })
});


//--------------------------------------------------Ruta de prueba(insertar)

apDispositivos.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('INSERT INTO ap_dispositivos SET ?' , [req.body], (err, rows) => {

            res.send('Se agrego exitosamente!')
        })
    })
});


//--------------------------------------------------Ruta de prueba(Eliminar)

apDispositivos.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM ap_dispositivos WHERE numero = ?', [req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se elimino exitosamente!')
        })
    })
});


//--------------------------------------------------Ruta de prueba(Actualizar)

apDispositivos.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err) 

        connection.query('UPDATE ap_dispositivos SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {

            res.send('Se actualizo exitosamente')
        })
    })
});


//---------------------------------------------------Exportar modulo
module.exports = apDispositivos