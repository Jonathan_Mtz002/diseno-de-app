const express = require('express');
const estados = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

estados.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM estados', (err, rows) => {
            if (err) return res.send(err)

            res.json(rows)
        })
    })
});

//--------------------------------------------------Ruta de prueba(insertar)

estados.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('INSERT INTO estados SET ?', [req.body], (err, rows) => {
            if (err) return res.send(err)

           res.send('Se registro exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

estados.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM estados WHERE numero = ?', [req.params.numero], (err, rows) => {
            if (err) return res.send(err)

           res.send('Se elimino exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Actualizar)

estados.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('UPDATE estados SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if (err) return res.send(err)

           res.send('Se actualizo exitosamente!')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = estados